import { divide } from 'lodash';
import React from 'react';
import SearchBar from '../src/components/SearchBar';


function App() {
  return (
    <div className = "App-Component">
      <p>Search MS ship model</p>
      <SearchBar/>
    </div>
  );
}

export default App;
