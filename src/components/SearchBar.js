import React from 'react';
import '../SearchBar.css';
import MagnifyingGlass from '../assets/magnifying-glass.svg';
import Cross from '../assets/cross.svg';

export default class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            suggestions: [],
            searchText: ''
        };
    }

    // Fetching data from the server
    getData(query) {
        return fetch('http://localhost:4000/api/ships/' + query)
            .then(res => res.json());
    }

    onTextChange = async (e) => {
        // Destructure the value
        const { value = '' } = e.target;

        // Every second letter send request
        if(value.length % 2 === 0) {
            let data = await this.getData(value);
            this.setState({suggestions : data, searchText: value});
        } else {
            this.setState({
                searchText: value,
            }) 
        }
    }

    suggestionSelected(value) {
        this.setState(() => ({
            searchText: value,
            suggestions: [],
        }));
    }


    renderSuggestions() {
        const { suggestions } = this.state;
        if (suggestions.length === 0) {
            return null;
        }
        return (
            <ul> 
                {suggestions.map((item) => <li onClick={() => this.suggestionSelected(item.name)}>{item.name}</li>)}
            </ul>
        );
    }

    // Press enter key function
    handleEnterPress = async (e) => {
        if(e.key === 'Enter') {
            let data = await this.getData(this.state.searchText);
            this.setState({suggestions : data});
        }
    }

    handleSearchClick = async (e) => {
        let data = await this.getData(this.state.searchText);
        this.setState({suggestions : data});
    }

    render() {
        const {suggestions} = this.state;
        return (
            <div className="SearchBar">
                <input placeholder="Search" onChange={(e) => this.onTextChange(e)} type="text" onKeyPress={this.handleEnterPress} value={this.state.searchText}/>
                {
                    this.state.searchText.length > 0 ?  (<img src={Cross} alt="Cross" onClick={() => this.setState({searchText : ''})}/>)
                    : (<img src={MagnifyingGlass} alt="MagnifyingGlass"/>) // Replacing magnifying glass to cross when input given
                }         
                <ul>
                    {suggestions.map(suggestion => <li key={suggestion.id} onClick={() => this.suggestionSelected(suggestion.name)}>{suggestion.name}</li>)}
                </ul>
                {this.renderSuggestions}
            </div>
        )
    }
}